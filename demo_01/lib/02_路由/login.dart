import 'package:flutter/material.dart';

class login extends StatefulWidget {
  // 3,在目标页面进行接收
  final Map arguments;
  login({
    super.key,
    required this.arguments,
  });
  @override
  State<login> createState() => _searchState();
}

class _searchState extends State<login> {
  // 打印接收路由参数
  @override
  void initState() {
    super.initState();
    print(widget.arguments);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('登录页面'),
      ),
      body: Center(
        child: Column(children: [
          ElevatedButton(
            onPressed: () {
              // 通过路由返回上一个页面
              Navigator.of(context).pop();
            },
            child: const Text('执行登录'),
          ),
        ]),
      ),
    );
  }
}
