import 'package:flutter/material.dart';
import '../widget/swiper.dart';

class PageViewSwiper extends StatefulWidget {
  const PageViewSwiper({super.key});
  @override
  State<PageViewSwiper> createState() => _PageViewSwiperState();
}

class _PageViewSwiperState extends State<PageViewSwiper> {
  List<String> list = [];
  @override
  void initState() {
    list = const [
      'https://www.itying.com/images/flutter/1.png',
      'https://www.itying.com/images/flutter/2.png',
      'https://www.itying.com/images/flutter/3.png',
      'https://www.itying.com/images/flutter/4.png',
    ];
  }

  void dispose() {
    super.dispose();
    // t.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("轮播"),
        ),
        // 使用轮播图组件并传入数据
        body: Swiper(list: list));
  }
}
