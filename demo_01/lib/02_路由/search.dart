import 'package:flutter/material.dart';
class search extends StatefulWidget {
  // 3,在目标页面进行接收
  final Map arguments;
  final String title;
  search({super.key, this.title = '搜索', required this.arguments,});
  @override
  State<search> createState() => _searchState();
}

class _searchState extends State<search> {
  // 打印接收路由参数
  @override
  void initState() {
    super.initState();
    print(widget.arguments);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Text('${widget.arguments["titlexx"]}'),
      ),
    );
  }
}