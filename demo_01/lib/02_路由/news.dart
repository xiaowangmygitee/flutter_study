import 'package:flutter/material.dart';

class news extends StatefulWidget {
  // 定义接收的变量
  final String title;
  final int aid;
  const news({super.key,this.title = '新闻', this.aid = 123});
  @override
  State<news> createState() => _newsState();
}

class _newsState extends State<news> {
  // 在生命周期钩子中可以访问传递的值
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.aid);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // 通过Navigator.of(context).pop()返回上一级
          Navigator.pop(context);
        },
        child: const Icon(Icons.arrow_back),
      ),
      appBar: AppBar(
        // 可以通过widget.title来获取传递过来的值
        title: Text(widget.title),
      ),
      body: const Center(
        child: Text('新闻页面'),
      ),
    );
  }
}
