import 'dart:isolate';
import 'dart:math';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  bool flag = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            flag = !flag;
          });
        },
        child: const Icon(Icons.opacity),
      ),
      appBar: AppBar(
        title: const Text("AnimatedPositioned Demo"),
      ),
      body: Center(
        // 使用 AnimatedPositioned 改变透明度时动画
        child: TweenAnimationBuilder(
          tween: Tween(begin: 0.5,end: flag? 0.5 : 1.0),
          duration: Duration(seconds: 1),
          builder: (context, value, child) {
            return Opacity(
              opacity: value as double,
              child: Container(
                width: 200.0,
                height: 200.0,
                color: Colors.red,
              ),
            );
          },
        ),
      ),
      // body: Center(
      //   // 使用 AnimatedPositioned 改变大小时动画
      //   child: TweenAnimationBuilder(
      //     tween: Tween(begin: 60.0,end: flag? 60.0 : 200.0),
      //     duration: Duration(seconds: 1),
      //     builder: (context, value, child) {
      //       return Icon(Icons.star,size: value as double,);
      //     },
      //   ),
      // ),
    );
  }
}
