import 'package:flutter/material.dart';
import '../tabs.dart';
import '../../02_路由/news.dart';
import '../../02_路由/search.dart';
import '../../02_路由/login.dart';
import '../../02_路由/pageView.dart';

// 1，定义路由对象
Map routes = {
  "/": (context) => Tabs(),
  "/news": (context) => news(),
  "/login": (context, {arguments}) {
    return login(arguments: arguments);
  },
  "/search": (context, {arguments}) {
    return search(arguments: arguments);
  },
  "/pageView": (context, {arguments}) {
    return PageViewSwiper();
  },
  // 简写
  // "/search": (context,arguments) => search(arguments: arguments),
};

var onGenerateRoute =(RouteSettings settings) {
        // print(settings);
        // print(settings.name);
        // print(settings.arguments);

        final String? name = settings.name; //  /news 或者 /search
        final Function? pageContentBuilder =
            routes[name]; //  Function = (contxt) { return const NewsPage()}

        if (pageContentBuilder != null) {
          if (settings.arguments != null) {
            final Route route = MaterialPageRoute(
                builder: (context) =>
                    pageContentBuilder(context, arguments: settings.arguments));
            return route;
          } else {
            final Route route = MaterialPageRoute(
                builder: (context) => pageContentBuilder(context));

            return route;
          }
        }
        return null;
      };
