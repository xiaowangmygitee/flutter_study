import 'package:flutter/material.dart';
import '../../tools/KeepAliveWrapper.dart';
import '../../02_路由/news.dart';
import '../../02_路由/search.dart';
import '../../02_路由/pageView.dart';
class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    // 1，监听tab改变事件
    _tabController.addListener(() {
      print(_tabController.index); // 获取两次，离开，进入
      // 这里加入判断，只获取一次
      if (_tabController.animation!.value == _tabController.index) {
        print(_tabController.index); //获取点击或滑动页面的索引值
      }
    });
  }
  // 页面销毁时，解除绑定
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _tabController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 在appbar中通过 PreferredSize 来设置导航栏的高度
      appBar: PreferredSize(
        // 设置AppBar高度
        preferredSize: Size.fromHeight(50),
        child: AppBar(
          // 导航栏底部边框
          elevation: 1,
          backgroundColor: Colors.white,
        // 直接配置顶部导航栏，则头部不会重复
        title: SizedBox(
          height: 40,
          child: TabBar(
            indicatorColor: Colors.red,
            labelColor: Colors.red,
            unselectedLabelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.label,
            controller: _tabController,
            // 2，监听导航栏切换,这里只能监听点击事件，无法监听滑动事件
            onTap: (value) {
              print('onTap: + ${value}');
            },
            tabs: const [
              Tab(
                icon: Icon(Icons.home),
              ),
              Tab(
                icon: Icon(Icons.category),
              ),
              Tab(
                icon: Icon(Icons.settings),
              ),
            ],
          ),
        )
      )),

      body: TabBarView(
        controller: _tabController,
        children: [
          Center(
            child: Column(children: [
              // 普通路由跳转
              // ElevatedButton(
              //   onPressed: () {
              //     // 通过该方法进行路由跳转
              //     Navigator.of(context)
              //         .push(MaterialPageRoute(builder: (context) {
              //       return search(title: "搜索",);
              //     }));
              //   },
              //   child: const Text('跳转搜索'),
              // ),
              ElevatedButton(
                onPressed: () {
                  // 通过该方法进行路由跳转
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return news(title: "新闻111", aid: 123);
                  }));
                },
                child: const Text('跳转news'),
              ),

              // 命名路由跳转
              ElevatedButton(
                onPressed: () {
                  // 通过该方法进行路由跳转
                  Navigator.pushNamed(context, "/login", arguments: {
                    "titlexx": "登录",
                  });
                },
                child: const Text('登录'),
              ),
              ElevatedButton(
                onPressed: () {
                  // 通过该方法进行路由跳转
                  Navigator.pushNamed(context, "/search", arguments: {
                    "titlexx": "注册",
                  });
                },
                child: const Text('注册'),
              ),
              ElevatedButton(
                onPressed: () {
                  // 通过该方法进行路由跳转
                  Navigator.pushNamed(context, "/pageView", arguments: {
                    "titlexx": "注册",
                  });
                },
                child: const Text('跳转轮播图'),
              ),

            ]),

          ),
          Center(child: Text('设置')),
          // 引入自定义的缓存组件，传入子组件
          KeepAliveWrapper(
            // 控制是否需要缓存
            keepAlive: false,
            child: ListView(
            children: const [
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('首页'),
              ),
              ListTile(
                leading: Icon(Icons.category),
                title: Text('分类'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('设置'),
              ),
            ],
          ),
          ),
        ],
      ),
    );
  }
}