import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../widget/myDialog.dart';
class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  // 定义弹窗方法
  Future<void> _alertDialog() async {
    var result = await showDialog(
      //表示点击灰色背景的时候是否消失弹出框
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("提示信息!"),
          content: const Text("您确定要删除吗?"),
          actions: <Widget>[
            TextButton(
              child: const Text("取消"),
              onPressed: () {
                print("取消");
                Navigator.pop(context, 'Cancel');
              },
            ),
            TextButton(
              child: const Text("确定"),
              onPressed: () {
                print("确定");
                Navigator.pop(context, "Ok");
              },
            ),
          ],
        );
      },
    );
    print(result);
  }

  // 定义弹出选择框
  _simpleDialog() async {
    var result = await showDialog(
        barrierDismissible: true, //表示点击灰色背景的时候是否消失弹出框
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: const Text("请选择内容"),
            children: <Widget>[
              SimpleDialogOption(
                child: const Text("Option A"),
                onPressed: () {
                  print("Option A");
                  Navigator.pop(context, "A");
                },
              ),
              const Divider(),
              SimpleDialogOption(
                child: const Text("Option B"),
                onPressed: () {
                  print("Option B");
                  Navigator.pop(context, "B");
                },
              ),
              const Divider(),
              SimpleDialogOption(
                child: const Text("Option C"),
                onPressed: () {
                  print("Option C");
                  Navigator.pop(context, "C");
                },
              ),
            ],
          );
        });
    print(result);
  }

  // 定义底部弹出框
  _modelBottomSheet() async {
    var result = await showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox( 
            height: 220,
            child: Column(
              children: <Widget>[
                ListTile(
                  title: const Text("分享 A"),
                  onTap: () {
                    Navigator.pop(context, "分享 A");
                  },
                ),
                const Divider(),
                ListTile(
                  title: const Text("分享 B"),
                  onTap: () {
                    Navigator.pop(context, "分享 B");
                  },
                ),
                const Divider(),
                ListTile(
                  title: const Text("分享 C"),
                  onTap: () {
                    Navigator.pop(context, "分享 C");
                  },
                )
              ],
            ),
          );
        });
    print(result);
  }

  // 第三方提示
  _toast() {
    Fluttertoast.showToast(
      msg: "This is Center Short Toast",
      toastLength: Toast.LENGTH_SHORT, // 只针对安卓平台的提示时间
      // toastLength: Toast.LENGTH_LONG, // 只针对安卓平台的提示时间
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1, // 提示时间只有在ios和web上面才生效
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0
    );
  }
  _myDialog() async{
    // 使用自定义弹窗需要 在 showDialog中使用
    var res = await showDialog(context: context, builder: (context){
      return MyDialog(
        title: "我是弹窗",
        content: '内容区域',
        onTap: () {
          Navigator.of(context).pop('我是点击关闭的事件');
        },
      );
    });
    print(res); // 我是点击关闭的事件
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Text("分类"),
        ElevatedButton(
            onPressed: _alertDialog, // 调用弹窗方法
            child: Text("点击弹窗")
        ),
        ElevatedButton(
            onPressed: _simpleDialog, // 调用弹窗方法
            child: Text("点击选择框")),
        ElevatedButton(
            onPressed: _modelBottomSheet, // 调用弹窗方法
            child: Text("点击底部弹出框")),
        ElevatedButton(
            onPressed: _toast, // 调用弹窗方法
            child: Text("第三方提示")),
            // 自定义弹窗
        ElevatedButton(
          onPressed: _myDialog, // 调用弹窗方法
          child: Text("自定义弹窗")
        ),
      ],
    ));
  }
}
