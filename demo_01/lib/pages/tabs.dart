import 'package:flutter/material.dart';
import './tabs/home.dart';
import './tabs/category.dart';
import './tabs/setting.dart';
import './tabs//user.dart';

class Tabs extends StatefulWidget {
  const Tabs({Key? key}) : super(key: key);
  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int homeIndex = 0;
  final List<Widget> _pages = const [
    HomePage(),
    CategoryPage(),
    SettingPage(),
    UserPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('首页'),
        centerTitle: true,
        backgroundColor: Colors.red,
        // leading: Icon(Icons.menu),
        // actions: [
        //   Icon(Icons.settings),
        // ],
      ),
      // 配置抽屉菜单栏 ,在Scaffold组件当中配置
      drawer: const Drawer(
        child: Column(children: [
          // 在抽屉组件的上方建议添加头部
          Row(
            children: [
              // 让头部占满
              Expanded(
                flex: 1,
                child: DrawerHeader(
                  decoration: BoxDecoration(
                      // color: Colors.blue,
                      image: DecorationImage(
                    image: NetworkImage(
                        "https://www.itying.com/images/flutter/2.png"),
                    fit: BoxFit.cover,
                  )),
                  child: Column(
                    children: [
                      ListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://www.itying.com/images/flutter/4.png"),
                        ),
                        title: Text(
                          '张三',
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),

          ListTile(
            leading: CircleAvatar(
              child: Icon(Icons.people),
            ),
            title: Text('首页'),
          )
        ]),
      ),
      endDrawer: Drawer(
        child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text("大地老师"),
            accountEmail: const Text("dadi@itying.com"),
            currentAccountPicture: const CircleAvatar(
              backgroundImage:
                  NetworkImage("https://www.itying.com/images/flutter/3.png"),
            ),
            decoration: const BoxDecoration(
                color: Colors.yellow,
                image: DecorationImage(
                    image: NetworkImage(
                        "https://www.itying.com/images/flutter/2.png"),
                    fit: BoxFit.cover)),
            otherAccountsPictures: <Widget>[
              Image.network("https://www.itying.com/images/flutter/4.png"),
              Image.network("https://www.itying.com/images/flutter/5.png"),
              Image.network("https://www.itying.com/images/flutter/6.png")
            ],
          ),
          const ListTile(
            title: Text("个人中心"),
            leading: CircleAvatar(child: Icon(Icons.people)),
          ),
          const Divider(),
          const ListTile(
            title: Text("系统设置"),
            leading: CircleAvatar(child: Icon(Icons.settings)),
          )
        ],
      )),

      // body: MyBottomNavigationBar(),
      body: _pages[homeIndex],
      // 在Scaffold 中配置底部导航栏
      bottomNavigationBar: BottomNavigationBar(
          // 选中的颜色
          fixedColor: Colors.red,
          // 设置底部导航图标的大小
          // iconSize: 40,
          // 设置选中 值为列表的下标
          currentIndex: homeIndex,
          // 监听导航栏的点击事件
          onTap: (value) {
            // 调用setState会重新build执行 重新赋值导航栏的下标
            setState(() {
              homeIndex = value;
            });
            print(value);
          },
          // 底部导航栏超过三个需要配置
          type: BottomNavigationBarType.fixed,
          // 配置底部导航栏的每一项
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: '首页',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.lock_clock),
              label: '分类',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.access_alarm),
              label: '我的',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.usb_outlined),
              label: '用户',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.usb_outlined),
              label: '用户',
            ),
          ]),
      // 设置浮标按钮
      floatingActionButton: Container(
        height: 60,
        width: 60,
        padding: EdgeInsets.all(5),
        // 调整位置
        margin: EdgeInsets.only(top: 7),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.black12,
        ),
        child: FloatingActionButton(
          // 更改背景色
          backgroundColor: homeIndex == 1 ? Colors.red : Colors.blue,
          onPressed: () {
            setState(() {
              homeIndex = 1;
            });
            print('点击了按钮');
          },
          child: Icon(Icons.add),
        ),
      ),
      // 设置浮标按钮的位置
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
