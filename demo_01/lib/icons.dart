import 'package:flutter/material.dart';

// 单独定义图标类
class IconsDemo {
  // 指定使用的图标编码及图标名
  static const IconData shuazi = IconData(59287, fontFamily: 'shuazi');
  static const IconData chitie = IconData(59291, fontFamily: 'chitie');
}

