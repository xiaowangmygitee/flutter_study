import 'package:flutter/material.dart';
import '../res/listData.dart';

class MyButton extends StatelessWidget {
  const MyButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          children: [
              Container(
                // 设置边距
                margin: EdgeInsets.all(10),
                // 设置按钮的宽高
                height: 90,
                child: OutlinedButton.icon(
                  // 设置按钮的边框
                style: ButtonStyle(
                  side: MaterialStateProperty.all(
                    const BorderSide(color: Colors.red,width: 2)
                  )
                ),
                icon: Icon(Icons.add),
                onPressed: () {
                  print('点击了图标按钮');
                },
                label: Text('lable'),
              )
            )
        ],),
      ],
    );
  }
}
