import 'package:flutter/material.dart';
import '../res/listData.dart';


// 有状态组件 继承StatefulWidget类
class MyWrap extends StatefulWidget {
  const MyWrap({super.key});
  @override
  // 使用StatefulWidget类需要实现它的createState方法，传入继承State的自定义类
  State<MyWrap> createState() => _MyWrapState();
}

class _MyWrapState extends State<MyWrap> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

		

    
	

