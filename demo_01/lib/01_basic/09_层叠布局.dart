import 'package:flutter/material.dart';

class MyStack extends StatelessWidget {
  const MyStack({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // 方式1：使用Align组件来实现
    return const Column(
      children: [
        SizedBox(
          width: double.infinity,
          height: 40,
          // Stack需要指定容器来进行定位布局
          child: Stack(
            children:  [
              Positioned(
                left: 0,
                child: Text('左边'),
              ),
              Positioned(
                right: 10,
                child: Text('右边'),
              ),
            ]
          ),
        )
      ],
    );
  }
  //   return const Stack(
  //     children:[
  //       Align(
  //         alignment: Alignment.topLeft,
  //         child: Text('左边'),
  //       ),
  //       Align(
  //         alignment: Alignment.topRight,
  //         child: Text('you边'),
  //       ),

  //     ],
  //   );
  // }
}
// class MyStack extends StatelessWidget {
//   const MyStack({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     // 当无法使用double.infinity的时候，在build方法中可以使用MediaQuery.of(context).size.width获取屏幕的宽度
//     final size = MediaQuery.of(context).size; 
//     // 使用Stack组件，当中的Positioned是相对于外层容器进行定位的，如果没有，则相对于整个屏幕进行定位
//     return Container(
//       // 1，调整内容的定位可以使用 Alignment
//       // alignment: Alignment.bottomRight,
//       width: 300,
//       height: 300,
//       color: Colors.red,
//       // 2，也可以使用Align组件来调整内容位置
//       child: const Align(
//         // alignment: Alignment.bottomCenter,
//         // 也可以传入参数进行定位
//         alignment: Alignment(-0.5,1),
//         child: Text('nihao,flutter'),
//       )

//       // 3，也可以直接使用Center组件直接实现居中定位
//       // child: const Center(
//       //   child: Text('nihao,flutter'),
//       // )
//     );
//   }
// }
// class MyStack extends StatelessWidget {
//   const MyStack({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     // 当无法使用double.infinity的时候，在build方法中可以使用MediaQuery.of(context).size.width获取屏幕的宽度
//     final size = MediaQuery.of(context).size;
//     // 使用Stack组件，当中的Positioned是相对于外层容器进行定位的，如果没有，则相对于整个屏幕进行定位
//     return Stack(
//       children: [
//         // 使用Positioned如果里面是行组件则需要指定子元素的宽度，无法使用double.infinity
//         Positioned(
//           top: 0,
//           width: size.width,
//           child: Row(
//           children: [
//             Expanded(
//               flex: 1,
//               child: Container(
//                 alignment: Alignment.center,
//                 height: 50,
//                 width: double.infinity,
//                 color: Colors.blueAccent,
//                 child: Text('二级导航'),
//               ),
//             ),
//           ],
//           )
//         ),
//         ListView(
//           padding: EdgeInsets.only(top: 50),
//           children:const [
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//             ListTile(
//               title: Text("标题"),
//               subtitle: Text("副标题"),
//             ),
//           ],
//         )
//       ],
//     );
//   }
// }
// class MyStack extends StatelessWidget {
//   const MyStack({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     // 使用Stack组件，当中的Positioned是相对于外层容器进行定位的，如果没有，则相对于整个屏幕进行定位
//     return Container(
//       height: 500,
//       width: 400,
//       color: Colors.indigo,
//       // Stack组件相对于外层容器
//       child: Stack(
//       // alignment设置子元素的显示位置
//       alignment: Alignment.topCenter,
//       children: [
//         // Positioned组件通过定位属性调整位置
//         Positioned(
//           bottom: 10,
//           right: 20,
//           child: Container(
//           height: 100,
//           width: 100,
//           color: Colors.redAccent,
//         ),    
//        ),
//       const Positioned(
//         top: 10,
//         left: 20,
//         child: Text('你好，flutter'),
//       ),
//       ],
//     )
//     );
//   }
// }
