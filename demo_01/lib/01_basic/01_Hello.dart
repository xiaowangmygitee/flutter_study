import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('首页'),
        centerTitle: true,
        leading: Icon(Icons.menu),
        actions: [
          Icon(Icons.settings),
        ],
      ),
      body: HelloFlutter(),
    );
  }
}
class HelloFlutter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Center(
      child: Container(
      width: 200,
      height: 200,
      // padding: EdgeInsets.only(left:50),
      decoration:BoxDecoration(
        color: Colors.blue,
        border: Border.all(color: Colors.red,width: 2),
        // borderRadius: BorderRadius.circular(100),
      ),
      // alignment:Alignment.center,
      child: const Text(
        'Hello Flutter,Hello Flutter,Hello Flutter,Hello Flutter',
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.right,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
          decoration: TextDecoration.lineThrough,
          decorationColor: Colors.blue,
          decorationStyle: TextDecorationStyle.double,
          decorationThickness: 2,
          height: 2,
        ),
      )
      )
    );
  }
}