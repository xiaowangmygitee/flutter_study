import 'package:flutter/material.dart';

class MyPadding extends StatelessWidget {
  const MyPadding ({Key? key}) : super(key: key);
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 1,
      children: [
        // 一些Widget是没有padding属性。这个时候
        // 我们可以用Padding组件处理容器与子元素之间的间距。
        Padding(
          // 使用PaddIng组件包裹并设置内边距
          padding: const EdgeInsets.all(50),
          child: Image.network('https://www.itying.com/images/flutter/1.png',
              fit: BoxFit.cover),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image.network('https://www.itying.com/images/flutter/1.png',
              fit: BoxFit.cover),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image.network('https://www.itying.com/images/flutter/2.png',
              fit: BoxFit.cover),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image.network('https://www.itying.com/images/flutter/3.png',
              fit: BoxFit.cover),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image.network('https://www.itying.com/images/flutter/4.png',
              fit: BoxFit.cover),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image.network('https://www.itying.com/images/flutter/1.png',
              fit: BoxFit.cover),
        ),
      ],
    );
  }
}