import 'package:flutter/material.dart';

class MyRow extends StatelessWidget {
  const MyRow({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // 设置整个容器的背景色
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black26,
      child: Column(
          // 主轴对排列方式 spaceBetween 两端对齐
          // spaceAround每个元素的两边距离相等,中间会有两个距离
          // spaceEvenly 所有的空白距离相等
          mainAxisAlignment: MainAxisAlignment.end,
          // 侧轴的排列顺序
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // 使用自定义组件，传入参数
            IconContainer(Icons.home, color: Colors.red),
            IconContainer(Icons.search, color: Colors.blue),
            IconContainer(Icons.send),
          ]),
    );
  }

}

// 自定义组件类
class IconContainer extends StatelessWidget {
  // 定义变量接收传递过来的参数
  Color color;
  double size;
  IconData icon;
  // 接收参数并赋值，其中赋默认值
  IconContainer(this.icon, {Key? key,this.color = Colors.red, this.size = 32}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // 返回一个容器
    return Container(
      height: 100.0,
      width: 100.0,
      color: color,
      // 容器中使用icon组件，并传入参数，生成不同的icon组件
      child: Center(child: Icon(icon, size: size, color: Colors.white)),
    );
  }
}



