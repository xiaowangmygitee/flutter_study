import 'package:flutter/material.dart';
import '../res/listData.dart';

class MyGridView extends StatelessWidget {
  MyGridView({Key? key}) : super(key: key);

   Widget _getListData(context, index){
      return Container(
        child: Column(
          children: [
            Image.network(listData[index]['imageUrl']),
            Text(listData[index]['title'])
          ],
        ),
      );
  }

  // Widget build(BuildContext context) {
  //   // GridView.builder 实现网格布局
  //   return GridView.builder(
  //     gridDelegate:  const SliverGridDelegateWithFixedCrossAxisCount(
  //       crossAxisSpacing: 10.0, //水平子 Widget 之间间距
  //       mainAxisSpacing: 10.0, //垂直子 Widget 之间间距
  //       crossAxisCount: 5, //一行的 Widget 数量
  //     ),
  //     // 设置固定数量的列表
  //     itemCount: listData.length,
  //     itemBuilder: _getListData,
  //   );
  // }
  Widget build(BuildContext context) {
    // GridView.builder 实现网格布局
    return GridView.builder(
      gridDelegate:  const SliverGridDelegateWithMaxCrossAxisExtent(
        crossAxisSpacing: 10.0, //水平子 Widget 之间间距
        mainAxisSpacing: 10.0, //垂直子 Widget 之间间距
        // 定义每个子Widget在交叉轴方向上的最大尺寸
        maxCrossAxisExtent: 150,
      ),
      // 设置固定数量的列表
      itemCount: listData.length,
      itemBuilder: _getListData,
    );
  }
}
