import 'package:flutter/material.dart';
import '../res/listData.dart';

class MyWrap extends StatelessWidget {
  const MyWrap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            Row(
              children: [
                Text(
                  "热搜",
                  style: Theme.of(context).textTheme.headline6,
                )
              ],
            ),
            const Divider(),
            Wrap(
              spacing: 10,
              runSpacing: 12,
              children: [
                Button("女装", onPressed: () {}),
                Button("笔记本", onPressed: () {}),
                Button("玩具", onPressed: () {}),
                Button("文学", onPressed: () {}),
                Button("女装", onPressed: () {}),
                Button("时尚", onPressed: () {}),
                Button("女装", onPressed: () {}),
                Button("女装", onPressed: () {}),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Text(
                  "历史记录",
                  style: Theme.of(context).textTheme.headline6,
                )
              ],
            ),
            const Divider(),
            
          ],
        ),
    );
  }
}

class Button extends StatelessWidget{
  String text;
  void Function()? onPressed;
   Button(this.text,{Key? key, required this.onPressed}) : super(key: key);
    @override
    Widget build(BuildContext context) {
      return ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Color.fromARGB(255, 151, 153, 155)),
          foregroundColor: MaterialStateProperty.all(Colors.blue)
        ),
        onPressed: onPressed,
        child: Text(text),
      );
  }
}
