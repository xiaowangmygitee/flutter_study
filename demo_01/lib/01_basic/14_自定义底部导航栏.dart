import 'package:flutter/material.dart';

class MyBottomNavigationBar extends StatelessWidget {
  const MyBottomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('底部导航栏'),
        centerTitle: true,
        leading: Icon(Icons.menu),
        actions: const [
         Icon(Icons.settings),
        ])
    );
  }
 }