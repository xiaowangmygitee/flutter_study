import 'package:flutter/material.dart';

class MyImage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Center(
      child: ClipOval(
        child: Image.asset("images/2.0x/57.jpg",
            width: 150.0, height: 150.0, fit: BoxFit.cover),
      ),
    );
  }
}
