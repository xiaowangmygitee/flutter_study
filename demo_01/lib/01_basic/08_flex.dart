import 'package:flutter/material.dart';

class MyFlex extends StatelessWidget {
  const MyFlex({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // 使用弹性组件Flex 来创建弹性布局
    return ListView(
      children: [
        Container(
          height: 200,
          width: double.infinity,
          color: Colors.black,
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              // Expanded组件大小以弹性盒子为准
              child: Container(
                height: 200,
                width: double.infinity,
                child: Image.network(
                  "https://www.itying.com/images/flutter/1.png",
                  fit: BoxFit.cover,
                ),
              )
            ),
            Expanded(
              flex: 1,
              // Expanded里面添加容器控制高度
              child: SizedBox(
                height: 200,
                child: Column(
                    children: [
                      Expanded(
                        flex: 3,
                        // 给图片添加容器，并设置高度，让图片填充
                        child: Container(
                          width: double.infinity,
                          child: Image.network(
                          "https://www.itying.com/images/flutter/2.png",
                          fit: BoxFit.cover,
                        ),
                        )
                      ),
                      Expanded(
                          flex: 1,
                          child: SizedBox(
                            width: double.infinity,
                            child: Image.network(
                              "https://www.itying.com/images/flutter/3.png",
                              fit: BoxFit.cover,
                            )),
                          )
                    ],
                  ))
            )
          ], 
        )
      ]);
    }
      
}



// class MyFlex extends StatelessWidget {
//   const MyFlex ({Key? key}) : super(key : key);
//   @override
//   Widget build(BuildContext context) {
//     // 使用弹性组件Flex 来创建弹性布局
//     return Flex(
//       // 垂直布局 horizontal(水平) vertical(垂直)
//       direction: Axis.vertical,
//       children: [
//         // 使用Expanded组件 来实现弹性盒子布局
//         Expanded(
//           flex: 1,
//           // Expanded组件大小以弹性盒子为准
//           child: IconContainer(Icons.home, color: Colors.red),
//         ),
//         // Expanded(
//         //   flex: 2,
//         //   child: IconContainer(Icons.search, color: Colors.blue),
//         // ),
        
       
//         IconContainer(Icons.send, color: Colors.blue),
//       ]
//     );
//   }
// }

// 自定义组件类
class IconContainer extends StatelessWidget {
  // 定义变量接收传递过来的参数
  Color color;
  double size;
  IconData icon;
  // 接收参数并赋值，其中赋默认值
  IconContainer(this.icon, {Key? key, this.color = Colors.red, this.size = 32})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // 返回一个容器
    return Container(
      height: 50.0,
      width: 50.0,
      color: color,
      // 容器中使用icon组件，并传入参数，生成不同的icon组件
      child: Center(child: Icon(icon, size: size, color: Colors.white)),
    );
  }
}
