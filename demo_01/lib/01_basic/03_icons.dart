import 'package:flutter/material.dart';
// 引入使用的图标类文件
import '../icons.dart';

class MyIcons extends StatelessWidget {
  const MyIcons({Key? key}) : super(key: key);
  Widget build(BuildContext context) {
    return const Center(
      child:  Column(
        children: [
          // 通过类名使用指定的图标
          Icon(IconsDemo.shuazi,size:30,color: Colors.green,),
          SizedBox(height: 10),
          Icon(
          IconsDemo.chitie,
          size: 30,
          color: Colors.red,
        ),
          
        ],
      )
    );
  }
}
