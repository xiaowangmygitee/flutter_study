import 'package:flutter/material.dart';
import '../res/listData.dart';


class MyListView extends StatelessWidget {
  MyListView({Key? key}) : super(key: key){
    print(listData[0]["title"]);
  }
  Widget build(BuildContext context) {
    // 使用ListView.builder方法生成动态列表
    // 需要传入两个参数 itemCount 长度  itemBuilder 函数接收两个参数
    return ListView.builder(
      itemCount: listData.length,
      // context, index 上下文对象和索引值
      itemBuilder: (context, index) {
        return ListTile(
          leading: Image.network(listData[index]["imageUrl"]),
          title: Text(listData[index]["title"]),
          subtitle: Text(listData[index]["author"]),
        );
      },
    ); 
  }
}

// class MyListView extends StatelessWidget {
//   const MyListView({Key? key}) : super(key: key);
//   Widget build(BuildContext context) {
//     // 一个横向列表容器
//     return SizedBox(
//       height: 180,
//       // ListView 创建列表组件
//       child: ListView(
//         // 设置列表方向
//         scrollDirection: Axis.horizontal,
//         children: [
//           // 设置每一个元素
//           Container(
//             width: 200,
//             // color: Colors.red,
//             // 容器里面存放一个垂直列表，当中展示图片和标题
//             child: Column(children: [
//               Image.network("https://www.itying.com/images/flutter/1.png"),
//               const Text('我是一个文本')
//             ],)
//           ),

//           Container(
//             width: 200,
//             // color: Colors.blue,
//               child: Column(
//                 children: [
//                   Image.network("https://www.itying.com/images/flutter/2.png"),
//                   const Text('我是一个文本')
//                 ],
//               )
//           ),
//           Container(
//             width: 200,
//             // color: Colors.green,
//               child: Column(
//                 children: [
//                   Image.network("https://www.itying.com/images/flutter/3.png"),
//                   const Text('我是一个文本')
//                 ],
//               )
//           )
//         ],
//       ),
//       );
//     }
// }
// class MyListView extends StatelessWidget {
//   const MyListView({Key? key}) : super(key: key);
//   Widget build(BuildContext context) {
//     // 使用ListView 创建列表组件
//     return ListView(
//       scrollDirection:Axis.vertical,
//       padding: const EdgeInsets.all(20),
//       children: [
//         Image.network("https://www.itying.com/images/flutter/1.png"),
//         Container(
//           height: 84,
//           padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
//           child: const Text(
//             '我是一个标题',
//             textAlign: TextAlign.left,
//             style: TextStyle(
//               fontSize: 18,
//             ),
//           ),
//           ),
//           Image.network("https://www.itying.com/images/flutter/2.png"),
//           Container(
//             height: 44,
//             padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
//             child: const Text(
//               '我是一个标题',
//               textAlign: TextAlign.center,
//               style: TextStyle(
//                 fontSize: 18,
//               ),
//             ),
//           ),
//           Image.network("https://www.itying.com/images/flutter/3.png"),
//           Container(
//             height: 44,
//             padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
//             child: const Text(
//               '我是一个标题',
//               textAlign: TextAlign.center,
//               style: TextStyle(
//                 fontSize: 18,
//               ),
//             ),
//           ),
//           Image.network("https://www.itying.com/images/flutter/4.png"),
//           Container(
//             height: 44,
//             padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
//             child: const Text(
//               '我是一个标题',
//               textAlign: TextAlign.center,
//               style: TextStyle(
//                 fontSize: 18,
//               ),
//             ),
//           ),
//       ]);
//     }
// }