import 'package:flutter/material.dart';
import '../res/listData.dart';

class MyCard extends StatelessWidget {
  const MyCard({Key? key}) : super(key: key);

  List<Widget> _getListData(){
    var tempList = listData.map((value){
      return Card(
          // 设置卡片圆角效果
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          elevation: 10,
          shadowColor: Colors.tealAccent,
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              AspectRatio(
                aspectRatio: 16/9,
                child: Image.network(value['imageUrl'],fit: BoxFit.cover),
              ),
              ListTile (
                // CircleAvatar 设置背景图实现圆形图片
                leading: CircleAvatar(
                  radius: 50,
                  backgroundImage: NetworkImage(value["imageUrl"])
                ),
                title: Text(value["title"]),
                subtitle: Text(value["author"]),
              )
            ],
          ),
        );
    });
    return tempList.toList();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _getListData()
    );
  }
}
