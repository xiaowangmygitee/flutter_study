import 'package:flutter/material.dart';

// 自定义弹窗继承Dialog类
class MyDialog extends Dialog {
  final String title;
  final String content;
  final Function()? onTap ;
  // const MyDialog({super.key,this.title = "标题"});
  MyDialog({Key? key, required this.title, required this.content, required this.onTap})
      : super(key: key);
  Widget build(context) {
    return Material(
      // 设置背景透明效果
      type: MaterialType.transparency,
      child: Center(
          child: Container(
        height: 200,
        width: 200,
        color: Colors.white,
        child: Column(children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(this.title),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    child: const Icon(Icons.close),
                    onTap: onTap,
                  ),
                ),
              ],
            ),
          ),
          const Divider(),
          Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            child: Text(content),
          )
        ]),
      )),
    );
  }
}
