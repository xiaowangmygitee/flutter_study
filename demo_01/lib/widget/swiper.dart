import 'package:flutter/material.dart';

import 'dart:async';

class ImagePage extends StatelessWidget {
  final double width;
  final double height;
  final String url;
  const ImagePage(
      {super.key,
      this.width = double.infinity,
      this.height = 200,
      required this.url});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Image.network(
        url,
        fit: BoxFit.cover,
      ),
    );
  }
}

class Swiper extends StatefulWidget {
  final double width;
  final double height;
  final List<String> list;
  const Swiper(
      {super.key,
      this.width = double.infinity,
      this.height = 200,
      required this.list});
  @override
  State<Swiper> createState() => _SwiperState();
}

class _SwiperState extends State<Swiper> {
  int _currentIndex = 0;
  List<Widget> pageList = [];
  // 1，定义PageController
  late PageController _pageController;
  late Timer timer;
  @override
  void initState() {
    super.initState();
    // 2，PageController定义初始化页面
    _pageController = PageController(initialPage: 0);
    for (var i = 0; i < widget.list.length; i++) {
      pageList.add(ImagePage(
        url: widget.list[i],
        width: double.infinity,
        height: 200,
      ));
    };
    // 创建定时器
    timer = Timer.periodic(Duration(seconds: 2), (timer) {
      print('执行了定时器');
      // 4,通过定时器调用PageController方法进行轮播
      _pageController.animateToPage((_currentIndex + 1) % pageList.length, duration: Duration(milliseconds: 500), curve: Curves.decelerate);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
            height: 200,
            child: PageView.builder(
              // 3，配置PageController
              controller: _pageController,
                onPageChanged: (index) {
                  setState(() {
                    _currentIndex = index % pageList.length;
                  });
                },
                itemCount: 1000,
                itemBuilder: (context, index) {
                  // index%list.length 实现无限轮播
                  return pageList[index % pageList.length];
                })),
        Positioned(
            left: 0,
            right: 0,
            bottom: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(pageList.length, (index) {
                return Container(
                  margin: EdgeInsets.all(5),
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                    color: _currentIndex == index ? Colors.blue : Colors.grey,
                    shape: BoxShape.circle,
                    // borderRadius: BorderRadius.circular(5)
                  ),
                );
              }),
            ))
      ],
    );
  }
}
