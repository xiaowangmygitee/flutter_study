// 引入系统内置数学库
import 'dart:math';
// core库会被默认引入
import 'dart:core';

void main() {
  print(pi); // 3.141592653589793 使用数学库当中的圆周率
  print(min(2, 9)); // 2
}
