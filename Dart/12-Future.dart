void main(List<String> args) {
  // 创建future实例
  final f = new Future(() {
    print('future');
    return 123;
  });

  print(f);

  // 异步方法
  f.then((value) => print(value));
}
