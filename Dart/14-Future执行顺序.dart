void main(List<String> args) {
  print(1);
  // 默认是一个异步操作,宏任务
  Future(() => print(2));
  // 同步任务
  Future.sync(() => print(3));
  // 微任务
  Future.microtask(() => print(4));
  print(10);

  // 1 3 10 4 2
}
