void main(List<String> args) {
  // 延迟2秒执行,第一个指定延迟时间，第二个指定执行回调
  Future.delayed(Duration(seconds: 2), () {
    // throw Error();  手动抛出一个报错
    return 123;
  }).then((value) {
    print(value);
  }).catchError((err) {
    print(err);
  }).whenComplete(() {
    print('完成时会执行的回调');
  });
}
