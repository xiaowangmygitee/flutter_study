void main() {
  // 将类对象用作函数名调用，会调用类当中的call方法
  var phone = IOSPhone();
  phone('123'); // Phone number is 123

  // IOSPhone()('bac'); 上面的方式可以简写
}

class IOSPhone {
  call(String num) {
    print('Phone number is $num');
  }
}
