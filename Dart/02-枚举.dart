void main() {
  // 获取索引值
  print(Color.blue.index);

  // 返回常量列表
  print(Color.values); // [Color.red, Color.reeen, Color.blue]

  var colors = Color.values;
  // 通过下标，访问列表中的内容
  print(colors[1]); // Color.reeen

  // 通过forEach去遍历列表的内容
  colors.forEach((element) {
    print('value: ${element},下标为：${element.index}');
  });
}

// 枚举，数量固定的常量值，enum
enum Color { red, reeen, blue }
