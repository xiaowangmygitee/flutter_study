// 增加了一个名为StringExtension的扩展，
// 它在String类上添加了一个名为parseInt()的方法。
extension StringExtension on String {
  // 将字符串形式的数值转换成数字
  parseInt() {
    return int.parse(this);
  }
}

void main() {
  // 1. 扩展类
  String str = "123";
  print(str.parseInt());
}
