// 定义一个抽象类接口
abstract class ObjCache {
  getBuykey(String key);
  void setBuykey(String key, String value);
}

abstract class StringCache {
  getBuykey(String key);
  void setBuykey(String key, String value);
}

// 泛型接口
abstract class Cache<T> {
  getBuykey(String key);
  void setBuykey(String key, T value);
}

class FileCache<T> implements Cache<T> {
  @override
  getBuykey(String key) {
    return null;
  }

  @override
  void setBuykey(String key, T value) {
    print('文件缓存：${key} ${value}');
  }
}

void main() {}
