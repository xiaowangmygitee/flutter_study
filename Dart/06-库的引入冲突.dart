// 两个库当中都存在f1方法，同时引入会发生冲突，
//通过as 取别名方式 进行调用
import 'LIb/03-fun1.dart' as lib1;
import 'LIb/04-fun2.dart' as lib2;

void main() {
  lib1.f1();
  lib2.f1();
}
