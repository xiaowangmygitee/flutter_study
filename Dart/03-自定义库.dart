import 'LIb/01-MyCustom.dart';
// 引入自定义的MYcustom库
// 路径可以写相对路径 ./ 可与省略不写

void main() {
  MYCustom m1 = new MYCustom();
  print(m1.name);
  m1.info();
  print(MYCustom.nn); // 123 访问静态属性
}
