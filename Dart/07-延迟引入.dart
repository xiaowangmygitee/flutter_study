// 使用延迟引入的方式，这时候无法加载f1方法
import 'LIb/02-common.dart' deferred as func;

void main() {
  // f1(); 这里无法调用异步加载的引入

  // 想要引入异步加载的库
  greet();
}

Future greet() async {
  await func.loadLibrary();
  func.f1();
}
