// 按需引入 12 ，无法使用3
import 'LIb/02-common.dart' show f1, f2;
// 排除 12，只能调用 3
import 'LIb/02-common.dart' hide f1, f2;

void main() {
  f1();
  f2();
  f3();
}
