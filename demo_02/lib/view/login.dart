import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // 1,定义 TextEditingController
  late TextEditingController _userNamecontroller;
  String userName = '张xx';
  String password = '';
  // 2,在initState中初始化TextEditingController
  @override
  void initState() {
    super.initState();
    _userNamecontroller = TextEditingController.fromValue(
      TextEditingValue(
        // 初始值
        text: userName,
        // 解决光标bug，flutter3当中已经修复，无需配置
        // selection: TextSelection.fromPosition(
        //   TextPosition(affinity: TextAffinity.downstream, offset: userName.length),
        // ),
      ),
    );
  }

// Controller 销毁
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _userNamecontroller.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // 通过Navigator.of(context).pop()返回上一级
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back),
        ),
        appBar: AppBar(
          // 可以通过widget.title来获取传递过来的值
          title: Text('登录'),
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            // 输入项表单
            TextField(
              // 3,配置到需要使用的地方
              controller: _userNamecontroller,
              // 表单描述
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.people),
                hintText: "请输入用户名",
                border: OutlineInputBorder(),
              ),
              // 表单方法 监听获取输入的值
              onChanged: (value) {
                setState(() {
                  userName = value;
                });
              },
            ),
            SizedBox(height: 20),
             TextField(
              obscureText: true,
              // 表单描述
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.lock),
                hintText: "请输入密码",
                border: OutlineInputBorder(),
              ),
              onChanged: (value) {
                setState(() {
                  password = value;
                });
              },
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                print("登录项的内容: + ${userName} + ${password}");
              },
              child: Text("登录"),
            )
          ],
        ));
  }
}
