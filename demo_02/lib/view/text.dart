import 'package:flutter/material.dart';

class TextPage extends StatefulWidget {
  const TextPage({super.key});

  @override
  State<TextPage> createState() => _TextPageState();
}

class _TextPageState extends State<TextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // 通过Navigator.of(context).pop()返回上一级
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back),
        ),
        appBar: AppBar(
          // 可以通过widget.title来获取传递过来的值
          title: Text('表单'),
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            // 输入项
            TextField(),
            SizedBox(height: 20),
            const TextField(
              decoration: InputDecoration(
                // 提示文本
                labelText: '用户名',
                // 聚焦时提示文本
                hintText: '请输入用户名',
                // 输入框里面前缀图标
                prefixIcon: Icon(Icons.person),
                // 输入框里面前缀文本
                prefixText: 'http://',
                // 输入框边框样式
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 20),

            const TextField(
              // 设置为密码框
              obscureText: true,
              decoration: InputDecoration(
                // 输入框外面前缀
                icon: Icon(Icons.add),
                labelText: '请输入密码',
                // 输入框边框样式
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 20),
            TextField(
              decoration: InputDecoration(
                // 输入框边框样式
                border: OutlineInputBorder(
                  // 配置圆角
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            SizedBox(height: 20),
            TextField(
              decoration: InputDecoration(
                labelText: '请输入关键词搜索',
                suffixIcon: Icon(Icons.search),
                // 输入框边框样式
                border: OutlineInputBorder(
                  // 配置圆角
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            SizedBox(height: 20),
            const TextField(
              // 设置多行文本框
              maxLines: 4,
              decoration: InputDecoration(
                labelText: '请输入关键词搜索',
                border: OutlineInputBorder(
                ),
              ),
            ),
          ],
        ));
  }
}
