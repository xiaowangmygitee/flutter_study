import 'package:flutter/material.dart';

class RadioPage extends StatefulWidget {
  const RadioPage({super.key});

  @override
  State<RadioPage> createState() => _RadioPageState();
}

class _RadioPageState extends State<RadioPage> {
  bool flag = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back),
        ),
        appBar: AppBar(
          title: Text('按钮组'),
        ),
        body: Center(
          child: Switch(
            value: flag,
            onChanged: (value){
              setState(() {
                flag = value;
              });
            }
          )
        )
    );
  }
}
