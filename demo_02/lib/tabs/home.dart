import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return  const Center(
      child:  Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // 加载滚动进度条
          CircularProgressIndicator(
            // 圆环的的颜色
            backgroundColor: Colors.cyan,
            // 圈圈的颜色
            valueColor: AlwaysStoppedAnimation(Colors.red),
          ),
          // 线性进度条
          LinearProgressIndicator(
            // 控制进度条的进度刻度
            value: 0.95,
            backgroundColor: Colors.green,
            valueColor: AlwaysStoppedAnimation(Colors.red),
          ),
          // ios当中的进度条,需要引入 import 'package:flutter/cupertino.dart';
          CupertinoActivityIndicator(
            radius: 20,
          ),
      ],)
    );
  }
}
