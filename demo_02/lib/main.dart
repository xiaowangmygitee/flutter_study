import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:dio/dio.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // 1,定义ScrollController
  final ScrollController _scrollController = ScrollController();
  List _list = [];
  int _page = 1;
  // 解决下拉多次重复请求的问题
  bool _isLoading = true;
  // 定义提示是否有数据
  bool _hasData = true;
  // 定义异步请求返回数据的方法
  _getData() async {
    if (_isLoading && _hasData) {
      _isLoading = false;
      var url =
          'https://www.phonegap100.com/appapi.php?a=getPortalList&catid=20&page=${_page}';
      print('重新执行了:${url}');
      var response = await Dio().get(url);
      // 转换成map对象
      var newsData = json.decode(response.data);
      print(newsData["result"][0]["title"]);
      if (newsData["result"].length < 20) {
        setState(() {
          _hasData = false;
        });
      }
      setState(() {
        // 拼接数组数据
        _list.addAll(newsData["result"]);
        _page++;
        _isLoading = true;
      });
    } else {}
  }

  @override
  void initState() {
    super.initState();
    _getData();
    // 3,监听滚动条滚动事件
    _scrollController.addListener(() {
      // _scrollController.position.pixels 获取滚动条下拉的高度
      // _scrollController.position.maxScrollExtent 获取页面的高度
      if (_scrollController.position.pixels >
          _scrollController.position.maxScrollExtent - 30) {
        _getData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _list.isNotEmpty
          ? RefreshIndicator(
              // RefreshIndicator 下拉刷新组件
              child: ListView.builder(
                controller: _scrollController,
                itemCount: _list.length,
                itemBuilder: ((context, index) {
                  if (index == _list.length - 1) {
                    return Column(
                      children: [
                        ListTile(
                          title: Text(_list[index]["title"]),
                        ),
                        _progressIndicator(),
                      ],
                    );
                  } else {
                    return ListTile(
                      title: Text(_list[index]["title"]),
                    );
                    // return CircularProgressIndicator();
                  }
                }),
              ),
              onRefresh: () async {
                // 在该方法中调用重新请求数据
                _getData();
              })
          : _progressIndicator(),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Increment',
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }

  //  自定义组件
  Widget _progressIndicator() {
    if (_hasData) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return const Center(
        child: Text('没有更多数据了'),
      );
    }
  }
}
